#!/bin/bash

echo 'Total Energy'
echo '------------'

echo 'CR-CC(2,3),A'
for i in $@
do
	grep -e 'CR-CC(2,3),A' $i | sed 's/\s\+/ /g' | cut -d ' ' -f 7
done

echo ''
echo 'CR-CC(2,3),D'
for i in $@
do
	grep -e 'CR-CC(2,3),D' $i | sed 's/\s\+/ /g' | cut -d ' ' -f 7
done
echo ''

echo 'Corrections'
echo '-----------'

echo 'CR-CC(2,3),A'
for i in $@
do
	grep -e 'CR-CC(2,3),A' $i | sed 's/\s\+/ /g' | sed 's/,//g' | cut -d ' ' -f 4
done

echo ''
echo 'CR-CC(2,3),D'
for i in $@
do
	grep -e 'CR-CC(2,3),D' $i | sed 's/\s\+/ /g' | sed 's/,//g' | cut -d ' ' -f 4
done
