#!/bin/bash

#for i in $(grep -oih 'reorder[0-9]\+' *.f); do echo -n "$i  - "; cat ~/code/cc_t3/UCCSDt/sub-sj/*.f | grep -c "$i"; done | grep -e "- 0$" > unused_reorder


# Find all subroutine
#for i in *.[fF] *.[fF]90; do grep -i subroutine $i | sed 's/\s\+subroutine //I' | grep -o '^\w\+(' | sed 's/($//' ; done > all_subs
for i in *.[fF]; do grep -i subroutine $i | sed 's/\s\+subroutine //I' | grep -o '^\w\+(' | sed 's/($//' ; done > all_subs

# Find unused subs
#while read p; do echo -n "$p  - "; cat *.[fF] | grep -ic "call\s\+$p"; done < all_subs  | grep '  - 0' > unused_subs
#while read p; do echo -n "$p  - "; cat *.[fF] *.[fF]90 | grep -Eic "(call|&)\s+$p\("; done < all_subs | tee uses
while read p; do echo -n "$p  - "; cat *.[fF] | grep -Eic "(call|&)\s+$p\("; done < all_subs | tee uses
grep '  - 0' uses | sed 's/  - 0/(/' > unused_subs

# Remove unused stuff
#while read p; do grep -c "$p" *.[fF] | grep '1$'; echo sed "/subroutine $p/,/end$/d" *.[fF] > /dev/null; done < unused_subs
#while read p; do sed -i "/subroutine $p/,/end$/d" *.[fF] *.[fF]90; done < unused_subs
#while read p; do sed -i "/subroutine $p/,/end$/d" *.[fF]; done < unused_subs

rm unused_subs uses all_subs
