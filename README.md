# qm_tools

My personal Quantum Chemistry tools and scripts. Specially useful are the scripts for generating files for stochastic CC(*P*;*Q*) calculations.

## Stochastic CC(P;Q) tools

This set of tools is used in the creation of integrals, symmetry files, and *P*
space files extracted from FCIQMC or CCMC simulations, to be ultimately used in
CC(*P*;*Q*) calculations.

### Usage

`read_cc_package.py` creates the FCIDUMP file required by HANDE for QMC
calculations and a symmetry file containing the molecular orbital symmetries.

`extract_p_space.py` extracts a given report out of a FCIQMC or CCMC
simulation performed in HANDE.

`write_p_space.py` writes down a file containing the *P* space for a given
excitation rank (e.g. triples, or triples and quadruples) to be used in
CC(*P*;*Q*) calculations.

