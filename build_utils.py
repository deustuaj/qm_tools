import sys
import re

# Constatns
R_TYPE = "real(kind=8) "


# Find reorders
def find_items(files):
    prog = re.compile("(reorder)(\d+)\(")
    reorder = list()
    for update in files:
        with open(update) as f:
            for line in f:
                res = prog.search(line)

                if res:
                    reorder.append(res.group(2))


    # Find sums
    prog = re.compile("(sum)(\d+)\(")
    sums = list()
    for update in files:
        with open(update) as f:
            for line in f:
                res = prog.search(line)

                if res:
                    sums.append(res.group(2))

    # Find sumxs
    prog = re.compile("(sumx)(\d+)")
    sumxs = list()
    for update in files:
        add_line = False
        with open(update) as f:
            for line in f:
                res = prog.search(line)

                if res:
                    sumxs.append(res.group(2))

    # Find sumxs_sorted
    prog = re.compile("(sumx_sorted)(\d+)")
    sumxs_sorted = list()
    for update in files:
        add_line = False
        with open(update) as f:
            for line in f:
                res = prog.search(line)

                if res:
                    sumxs_sorted.append(res.group(2))


    # Find sumxs_sorted
    reorder = set(reorder)
    sums = set(sums)
    sumxs = set(sumxs)
    sumxs_sorted = set(sumxs_sorted)

    return reorder, sums, sumxs, sumxs_sorted


class Writer:

    def __init__(self, f_handle):

        self.f = f_handle
        self.fix_sp = 6

    def declare(self, routine):
        out_str = self.fix_sp * " " + routine
        self.f.write(out_str)

    def split_lines(self, string):
        out_str = ""
        str_len = len(string)
        while str_len > 70:
            pos = string[0:70-self.fix_sp].rfind(",") + 1
            if pos == -1:
                pos = string[0:70-self.fix_sp].rfind("+") + 1


            out_str += string[0:pos] + "\n" + \
                    (self.fix_sp-1) * " " + "& "
            string = string[pos:]

            str_len = len(string) + self.fix_sp + 1

        out_str += string

        return out_str

    def print_fix(self, string):
        out_str = self.fix_sp * " " + string
        str_len = len(out_str)
        if str_len > 70:
            out_str = self.split_lines(out_str)

        self.f.write(out_str + "\n")

    def print_vspace(self, count):
        out_str = "\n" * count
        self.f.write(out_str)



def gen_bounds(count):
    # Generated boundaries
    A = list()
    B = list()
    inds = list()
    for j in range(count):
        A.append(("As{}".format(j+1), "Ae{}".format(j+1)))
        B.append(("Bs{}".format(j+1), "Be{}".format(j+1)))
        inds.append("i{}".format(j+1))

    return A, B, inds

def str_int_bounds(A):
    first = True
    out_str = "integer "
    for bounds in A:
        if first:
            out_str +="{},{}".format(bounds[0], bounds[1])
            first = False
        else:
            out_str += ",{},{}".format(bounds[0], bounds[1])

    return out_str

def str_int_inds(inds):
    return "integer " + str_inds(inds)

def str_inds(inds):
    out_str = ""
    first = True
    for indx in inds:
        if first:
            out_str += "{}".format(indx)
            first = False
        else:
            out_str += ",{}".format(indx)

    return out_str

def str_arr_bounds(name, A):
    first = True
    out_str = R_TYPE + name + "("
    for bounds in A:
        if first:
            out_str += "{}+1:{}".format(bounds[0], bounds[1])
            first = False
        else:
            out_str += ",{}+1:{}".format(bounds[0], bounds[1])

    out_str += ")"
    return out_str


def gen_reorder(w, reorder):
    for reor_perm in reorder:
        # Generate boundaries
        A, B, inds = gen_bounds(len(reor_perm))
        B_perm = [B[int(j) - 1] for j in reor_perm]
        inds_perm = [inds[int(j) - 1] for j in reor_perm]

        # Write subroutine interface
        # --------------------------
        out_str = "subroutine reorder{}(".format(reor_perm)

        for bounds in A:
            out_str += "{},{},".format(bounds[0], bounds[1])

        for bounds in B_perm:
            out_str += "{},{},".format(bounds[0], bounds[1])

        out_str += "A, B)"
        w.print_fix(out_str)
        w.print_vspace(1)

        w.print_fix("implicit none")
        w.print_vspace(1)


        # Declare integers
        # ----------------
        w.print_fix(str_int_bounds(A))
        w.print_fix(str_int_bounds(B))
        w.print_fix(str_int_inds(inds))
        w.print_vspace(1)

        # Declare arrays
        # --------------

        w.print_fix(str_arr_bounds("A", A))
        w.print_fix(str_arr_bounds("B", B_perm))
        w.print_vspace(1)


        # Create loops
        # ------------
        for indx, bounds in zip(inds, B):
            w.print_fix("do {}={}+1,{}".format(indx, bounds[0], bounds[1]))

        out_str = "B("
        out_str += str_inds(inds_perm)
        out_str += ")=A("
        out_str += str_inds(inds)
        out_str += ")"
        w.print_fix(out_str)

        for indx in inds:
            w.print_fix("enddo")

        w.print_vspace(1)
        w.print_fix("end")
        w.print_vspace(2)

def gen_sums(w, sums):
    for sum_perm in sums:
        # Generate boundaries
        A, B, inds = gen_bounds(len(sum_perm))
        A_perm = [A[int(j) - 1] for j in sum_perm]
        inds_perm = [inds[int(j) - 1] for j in sum_perm]

        # Write subroutine interface
        # --------------------------
        out_str = "subroutine sum{}(".format(sum_perm)

        for bounds in A:
            out_str += "{},{},".format(bounds[0], bounds[1])

        out_str += "A, B, C)"
        w.print_fix(out_str)
        w.print_vspace(1)

        w.print_fix("implicit none")
        w.print_vspace(1)

        # Declare integers
        # ----------------
        w.print_fix(str_int_bounds(A))
        w.print_fix(str_int_inds(inds))
        w.print_vspace(1)

        # Declare arrays
        # --------------
        w.print_fix(str_arr_bounds("A", A))
        w.print_fix(str_arr_bounds("B", A_perm))
        #w.print_fix(R_TYPE + "C")
        w.print_fix("real C")
        w.print_vspace(1)

        # Create loops
        # ------------
        for indx, bounds in zip(inds, A):
            w.print_fix("do {}={}+1,{}".format(indx, bounds[0], bounds[1]))

        a_arr = "A(" + str_inds(inds) + ")"
        out_str = a_arr + "=" + a_arr
        out_str += "+C*B(" + str_inds(inds_perm) + ")"
        w.print_fix(out_str)


        for indx in inds:
            w.print_fix("enddo")

        w.print_vspace(1)
        w.print_fix("end")
        w.print_vspace(2)

def gen_sumxs(w, sumxs):
    for sumx_perm in sumxs:
        # Generate boundaries
        A, B, inds = gen_bounds(len(sumx_perm))
        B = [("Bs", "Be")]
        A_perm = [A[int(j) - 1] for j in sumx_perm]
        inds_perm = [inds[int(j) - 1] for j in sumx_perm]

        # Write subroutine interface
        # --------------------------
        out_str = "subroutine sumx{}(".format(sumx_perm)

        for bounds in B:
            out_str += "{},{},".format(bounds[0], bounds[1])

        for bounds in A:
            out_str += "{},{},".format(bounds[0], bounds[1])

        out_str += "A, B, C)"
        w.print_fix(out_str)
        w.print_vspace(1)

        w.print_fix("implicit none")
        w.print_vspace(1)

        # Declare integers
        # ----------------
        w.print_fix(str_int_bounds(B))
        w.print_fix(str_int_bounds(A))
        w.print_fix(str_int_inds(inds))
        w.print_vspace(1)

        # Declare arrays
        # --------------
        w.print_fix(str_arr_bounds("A", A))
        B_tmp = len(A) * B
        w.print_fix(str_arr_bounds("B", B_tmp))
        #w.print_fix(R_TYPE + "C")
        w.print_fix("real C")
        w.print_vspace(1)

        # Create loops
        # ------------
        for indx, bounds in zip(inds, A):
            w.print_fix("do {}={}+1,{}".format(indx, bounds[0], bounds[1]))

        a_arr = "A(" + str_inds(inds) + ")"
        out_str = a_arr + "=" + a_arr
        out_str += "+C*B(" + str_inds(inds_perm) + ")"
        w.print_fix(out_str)


        for indx in inds:
            w.print_fix("enddo")

        w.print_vspace(1)
        w.print_fix("end")
        w.print_vspace(2)

def gen_sumxs_sorted(w, sumxs):
    for sumx_perm in sumxs:
        # Generate boundaries
        if len(sumx_perm) == 1:
            A, B, inds = gen_bounds(int(sumx_perm) * 2)
            inds_perm = inds
        else:
            A, B, inds = gen_bounds(len(sumx_perm))
            inds_perm = [inds[int(j) - 1] for j in sumx_perm]

        # Write subroutine interface
        # --------------------------
        out_str = "subroutine sumx_sorted{}(".format(sumx_perm)

        for bounds in A:
            out_str += "{},{},".format(bounds[0], bounds[1])

        for bounds in B:
            out_str += "{},{},".format(bounds[0], bounds[1])

        out_str += "A, B, C)"
        w.print_fix(out_str)
        w.print_vspace(1)

        w.print_fix("implicit none")
        w.print_vspace(1)

        # Declare integers
        # ----------------
        w.print_fix(str_int_bounds(A))
        w.print_fix(str_int_bounds(B))
        w.print_fix(str_int_inds(inds))
        w.print_vspace(1)

        # Declare arrays
        # --------------
        if len(sumx_perm) == 1:
            w.print_fix(str_arr_bounds("A", A))
            w.print_fix(str_arr_bounds("B", B))
        else:
            w.print_fix(str_arr_bounds("A", B))
            w.print_fix(str_arr_bounds("B", A))
        #w.print_fix(R_TYPE + "C")
        w.print_fix("real C")
        w.print_vspace(1)

        # Create loops
        # ------------
        for indx, bounds in zip(inds, B):
            w.print_fix("do {}={}+1,{}".format(indx, bounds[0], bounds[1]))

        a_arr = "A(" + str_inds(inds) + ")"
        out_str = a_arr + "=" + a_arr
        out_str += "+C*B(" + str_inds(inds_perm) + ")"
        w.print_fix(out_str)


        for indx in inds:
            w.print_fix("enddo")

        w.print_vspace(1)
        w.print_fix("end")
        w.print_vspace(2)

with open("cc_mat_man.f", "w") as f:

    # Find iterms
    reorder, sums, sumxs, sumxs_sorted = find_items(sys.argv[1:])
    #sys.exit()

    # Init writier
    w = Writer(f)

    # Generate reorder
    gen_reorder(w, reorder)

    # Generate sums
    gen_sums(w, sums)

    # Generate sumxs
    gen_sumxs(w, sumxs)

    # Generate sumxs_sorted
    gen_sumxs_sorted(w, sumxs_sorted)
