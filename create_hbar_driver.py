import re
import sys
import glob
from shutil import copy

EQ_FOLDER = \
"/scratch/hbar_new_autocode/hbar_derive_test_opt/HBar/new_test/"

class Line:

    PROJ_OCC_ORBS = "ijkl"
    PROJ_UNOCC_ORBS = "abcd"

    CON_OCC_ORBS = "mnop"
    CON_UNOCC_ORBS = "efgh"

    ORBS = "pqrs"

    def __init__(self, indx, spin, internal=False, op_type=None,
            occ_type=None):

        self.spin = spin
        self._deduce_line(indx)

        if op_type:
            self.op_type = op_type

        if occ_type:
            self.occ_type = occ_type

    def _deduce_line(self, indx):
        self.indx = indx

        if indx in Line.PROJ_OCC_ORBS:
            self.internal = False
            self.op_type = 'a'
            self.occ_type = 'o'

        elif indx in Line.PROJ_UNOCC_ORBS:
            self.internal = False
            self.op_type = 'c'
            self.occ_type = 'u'

        elif indx in Line.CON_OCC_ORBS:
            self.internal = True
            self.op_type = 'a'
            self.occ_type = 'o'

        elif indx in Line.CON_UNOCC_ORBS:
            self.internal = True
            self.op_type = 'c'
            self.occ_type = 'u'

    def __str__(self):
        if self.spin == 'a':
            return "{}_{}".format(self.op_type, self.indx)
        else:
            return "{}_~{}".format(self.op_type, self.indx)

    def __repr__(self):
        return self.__str__()

class Hamiltonian:

    def __init__(self, string):
        self._decode(string)

    def _decode(self, string):
        res = re.search(r'<(.+)\|\\bar{([A-z])}\|(.+)>', string)

        self._name = res.group(2)

        self._bra = get_braket(res.group(1))
        self._ket = get_braket(res.group(3))

        self._rank = int(len(self._bra + self._ket) / 2)

    @property
    def name(self):
        return self._name

    @property
    def bra(self):
        return self._bra

    @property
    def ket(self):
        return self._ket


class Call:

    def __init__(self, res, hbar, hbar_line):
        self._hbar = hbar
        self._name = res.group(1)
        self._dims = res.group(2)
        self._full = res.group(0)
        self.hamiltonian = Hamiltonian(hbar_line)

        self._decode_hbar()

    def _decode_hbar(self):
        self._rank = int((int(self._name[0]) + int(self._name[2]))/2)
        cnt_spin = int(self._full.count('N2') / 2)
        self._spin = chr(65 + cnt_spin)

    @property
    def hbar(self):
        return self._hbar

    @property
    def name(self):
        return self._name

    @property
    def dims(self):
        return self._dims

    @property
    def full(self):
        return self._full

    @property
    def rank(self):
        return self._rank

    @property
    def spin(self):
        return self._spin

class Printing:

    def __init__(self, filename=None):
        if filename:
            self._filename = filename
            self._f_out = open(filename, 'w')
        else:
            self._f_out = None

    def print(self, string):
        if self._f_out:
            self._f_out.write(string + "\n")
        else:
            print(string)

    def close(self):
        if self._f_out:
            self._f_out.close()

def to_f90(lines):
    newlines = []
    sub_res = re.compile("subroutine ([\d\w]+)\(")
    sub_real = re.compile("real\*8")
    sub_integer = re.compile("integer")

    for indx, line in enumerate(lines):
        try:
            next_line = lines[indx+1]
        except IndexError:
            pass

        res = sub_res.search(line)
        if res:
            sub = res.group(1)

        if not "allocatable" in line:
            line = sub_real.sub("real(kind=8) ::", line)
        else:
            line = sub_real.sub("real(kind=8)", line)
        line = sub_integer.sub("integer ::", line)

        if '&' in next_line:
            newlines.append((indx, line.replace('&', '').replace('\n', ' &\n')))
        elif '&' in line:
            newlines.append((indx, line.replace('&', '')))
        else:
            if line[0] == 'C':
                newlines.append((indx, line.replace('C', '')))
            elif "end" in line:
                newlines.append((indx, "end subroutine {}\n".format(sub)))
            else:
                newlines.append((indx, line))

    assert indx + 1 == len(lines), "Lines don't match"

    res = [x[1] for x in newlines]
    return res

def get_braket(string):

    alpha = re.compile(r'([a-z]+)')
    beta = re.compile(r'\\tilde{([a-z])}')

    res = beta.findall(string)
    lines_beta = []
    for indx in res:
        lines_beta.append(Line(indx, 'b'))

    rem_beta = beta.sub("", string)
    res = alpha.search(rem_beta)
    lines_alpha = []
    if res:
        for indx in res.group(1):
            lines_alpha.append(Line(indx, spin='a'))

    return lines_alpha + lines_beta

def parse_files(files):

    calls = []
    all_lines = []
    for in_file in files:

        with open(in_file) as f_in:
            lines = f_in.readlines()

        all_lines += lines

        for line in lines:
            res = re.search("HBar\d[ABCDE]\d[ABCDE]\d?", line)
            if res:
                hbar = res.group(0)

            res = re.search("V(\d[ABCDE]\d[ABCDE])\((.*)\)$", line)
            if res:
                hbar_line = get_hbar_line(hbar)
                calls.append(Call(res, hbar, hbar_line))
                break

        all_lines.append("\n")

    return calls, all_lines

def get_hbar_line(hbar):

    try:
        filename = hbar[8] + "/R" + hbar[4:8] + ".tex"
    except IndexError:
        filename = "R" + hbar[4:8] + ".tex"

    with open(EQ_FOLDER + filename) as f_in:
        for line in f_in:
            if "title" in line:
                continue

            if r"\bar{H}" in line:
                return line.strip()

def gen_hbar_def(max_rank):
    res = ""
    for rank in range(max_rank):
        for spin in range(rank + 2):
            res += "    real(kind=8) :: H{}{}".format(rank + 1,
                    chr(65 + spin))

            dims = ""
            first = True
            for dim in range((rank+1)*2):
                if first:
                    dims += "N0+1:N3"
                    first = False
                else:
                    dims += ",N0+1:N3"

            res += "(" + dims + ")\n"

    return res

def write_file(calls, files):

    #fout = Printing(filename='hbar.f90')
    fout = Printing()

    pre_str = preamble()

    fout.print(pre_str)

    hbars = gen_hbar_def(3)
    fout.print(hbars)

    defs = set([call.name for call in calls])
    defs_str = form_defs(defs)


    fout.print(defs_str)

    for call in calls:
        call_str = form_call(call)
        fout.print(call_str)
        fout.print("")

    fout.print("end subroutine update_hbar\n")

    for lines in files:
        fout.print(lines.strip())

    fout.close()

def preamble():

    res = "subroutine update_hbar(N0, N1, N2, N3, &\n"
    res += "        fockr, fockb, intr, intb, intm, &\n"
    res += "        t1a, t1b, t2b, t2c, t3a, t3c, t3d)\n\n"
    res += "    integer :: i, j, k, l\n"
    res += "    integer :: a, b, c, d\n"


    return res

def form_defs(defs):

    res = ""
    for defi in defs:
        dims = int(defi[0]) + int(defi[2])
        dims_str = ":," * dims

        res += "    real(kind=8), allocatable :: V{}({})\n".format(defi,
                dims_str[0:-1])

    return res


def form_call(call):
    res = "    allocate({})\n".format(call.full)
    res += "    call {}".format(call.hbar) + \
            "(N0, N1, N2, N3, V{}, &\n".format(call.name) + \
            "        k1, k2, k3, k4, &\n" + \
            "        fockr, fockb, intr, intb, intm, &\n" + \
            "        t1a, t1b, t2a, t2b, t2c, t3a, t3b, t3c, t3d)\n"
    res += "\n"

    occs = iter("ijkl")
    unoccs = iter("abcd")

    dos = call.dims.split(",")
    dos = [x.replace(":", ",") for x in dos]
    dos.reverse()

    inds = []
    for do in dos:
        if "N3" in do:
            indx = next(unoccs)
        else:
            indx = next(occs)

        inds.append(indx)
        res += "    do {}={}\n".format(indx, do)

    bra = [x.indx for x in call.hamiltonian.bra]
    ket = [x.indx for x in call.hamiltonian.ket]

    bra.reverse()
    ket.reverse()

    inds.reverse()
    form = "(" + "{}," * (len(inds) - 1) + "{})"
    ind_str = form.format(*inds)
    ham_ind_str = form.format(*bra, *ket)

    res += "        H{}{}".format(call.rank, call.spin)
    res += ham_ind_str + " = V{}".format(call.name) + ind_str + "\n"

    for do in dos:
            res += "    enddo\n"


    res += "\n"
    res += "    deallocate(V{})".format(call.name)

    return res

all_files = glob.glob(EQ_FOLDER + "/tex/for/*.f")
all_files += glob.glob(EQ_FOLDER + "/0/tex/for/*.f")
all_files += glob.glob(EQ_FOLDER + "/1/tex/for/*.f")

for i in all_files:
    copy(i, ".")

all_files = glob.glob("*.f")

calls, files = parse_files(all_files)
all_lines = to_f90(files)
write_file(calls, all_lines)
