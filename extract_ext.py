#!/bin/python3

import numpy as np
import matplotlib.pyplot as plt
import collections as col
import subprocess
import sys
import re
import itertools as it


def read_qmc(filename):
    read = False
    it = col.deque()
    corr = col.deque()
    with open(filename,"r") as qmc_f:
        for line in qmc_f:
            if "#              0   0.0000000000E+00" in line:
                read = True
                continue

            if read:
                if len(line) < 2 or "# --------" in line:
                    read = False
                    continue

            if read:
                fields = line.split()
                it.append(float(fields[0]))
                corr_e = float(fields[2])/float(fields[3])
                corr.append(corr_e)

    return it, corr


def read_ext_corr(files):
    it = col.deque()
    corr = col.deque()
    dis = col.deque()
    dis_qmc = col.deque()
    for filename in files:
        file_it = re.search(r"[0-9]{4}", filename)
        it.append(int(file_it[0])*20)
        with open(filename,"r") as ext_f:
            for line in ext_f:
                if "E(Ref)" in line:
                    fields = line.split()
                    hf = float(fields[1])

                elif "E(CCSD(TQ)M" in line:
                    fields = line.split()
                    corr.append(float(fields[1]) - hf)
        # Externally corrected
        filename_moe = filename.replace(".out", ".moe")

        comp_exe = "/home/edeustua/code/compare_triples/compare_t3"
        #comp_file = "/scratch/compare_vecs/fci_corr_hacked.moe"
        comp_file = "/scratch/compare_vecs/fci_ext_corr_hacked.moe"
        #comp_file = "/home/edeustua/Downloads/hande/test_eds/ext_corr/H2O-cc-pVDZ-2.0/ewalkers-9950.moe"
        #out = subprocess.Popen(comp_exe+" -dis -ns "+filename_moe+" "+comp_file,
        #        shell=True, stdout=subprocess.PIPE).stdout.read()

        #last_line = out.splitlines()[-1].decode('utf-8')
        #fields = last_line.split(':')

        #try:
        #    dis.append(float(fields[1]))
        #except IndexError:
        #    pass

        # Externally corrected
        #filename_moe = filename.replace(".out", "_synthetic.moe")
        #out = subprocess.Popen(comp_exe+" -dis "+filename_moe+" "+comp_file,
        #        shell=True, stdout=subprocess.PIPE).stdout.read()

        #last_line = out.splitlines()[-1].decode('utf-8')
        #fields = last_line.split(':')

        #try:
        #    dis_qmc.append(float(fields[1]))
        #except IndexError:
        #    pass


    return it, corr, dis, dis_qmc


def main():
    qmc_it, qmc_corr = read_qmc(sys.argv[1])
    ext_it, ext_corr, dis, dis_qmc = read_ext_corr(sys.argv[2:])

    f, (ax1, ax2) = plt.subplots(2,1,sharex=True)
    ax1.plot(qmc_it,qmc_corr,label="QMC")
    if len(ext_it) > len(ext_corr):
        ext_it_qmc = list(it.islice(ext_it, 0, len(ext_corr)))
        ax1.plot(ext_it_qmc,ext_corr,'r',label="EXT")
        #ax2.plot(ext_it,dis,label="DIS")
    else:
        ax1.plot(ext_it,ext_corr,'r',label="EXT")
        #ax2.plot(ext_it,dis,label="DIS")

    if len(ext_it) < len(dis_qmc):
        dis_qmc = list(it.islice(dis_qmc, 0, len(ext_it)))

    ext_it_qmc = list(it.islice(ext_it, 0, len(dis_qmc)))
    #ax2.plot(ext_it_qmc,dis_qmc,label="DIS QMC")
    ax1.legend()
    #ax2.legend()

    plt.show()

if __name__ == "__main__":
    main()


