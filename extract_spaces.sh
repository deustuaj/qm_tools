#!/bin/bash

#iterations=( 100 500 1000 1500 2000 2500 3000 3500 4000 5000 6000 )
iterations=( 2000 10000 20000 30000 40000 50000 60000 70000 80000 100000 120000 )

for i in ${iterations[@]}
do
    grep -m 1 -e "  $i" $1
done
