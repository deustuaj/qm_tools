#!/bin/bash

echo 'CCSDT(P)'
for i in $@
do
	grep -e 'E(CCSD' $i | sed 's/\s\+/ /g' | cut -d ' ' -f 3
done
