import sys
import os
import re
import glob
from fractions import Fraction

CONFIG = { 'spins': ('A', 'B', 'C', 'D', 'E') }

class Line:

    PROJ_OCC_ORBS = "ijkl"
    PROJ_UNOCC_ORBS = "abcd"

    CON_OCC_ORBS = "mnop"
    CON_UNOCC_ORBS = "efgh"

    ORBS = "pqrs"

    def __init__(self, indx, spin, internal=False, op_type=None,
            occ_type=None):

        self.spin = spin
        self._deduce_line(indx)

        if op_type:
            self.op_type = op_type

        if occ_type:
            self.occ_type = occ_type

    def _deduce_line(self, indx):
        self.indx = indx

        if indx in Line.PROJ_OCC_ORBS:
            self.internal = False
            self.op_type = 'a'
            self.occ_type = 'o'

        elif indx in Line.PROJ_UNOCC_ORBS:
            self.internal = False
            self.op_type = 'c'
            self.occ_type = 'u'

        elif indx in Line.CON_OCC_ORBS:
            self.internal = True
            self.op_type = 'a'
            self.occ_type = 'o'

        elif indx in Line.CON_UNOCC_ORBS:
            self.internal = True
            self.op_type = 'c'
            self.occ_type = 'u'

    def __str__(self):
        return self.indx

    def __repr__(self):
        return self.__str__()

class Operator:

    def __init__(self, string):
        self.string = string.strip()
        self._decode(string)
        #self._get_id()

    def _decode(self, string):
        res = re.search(r'<(.+)\|\\bar{([A-z])}\|(.+)>', string)

        if (res):
            self._decode_hbar(res)
        else:
            res = re.search(r'(\\bar{[FV]}|r|t)_{(\d?[A-Z])+}\(([a-z]+)\)',
                    string)
            if res:
                self._decode_excitation(res)

    def _decode_excitation(self, res):

        self.name = res.group(1)
        if 'F' in self.name:
            #self.name = 'F'
            self.rank = 1
            spin_type = self.rank - (ord(res.group(2)[0]) - 65)
        elif 'V' in self.name:
            #self.name = 'V'
            self.rank = 2
            spin_type = self.rank - (ord(res.group(2)[0]) - 65)
        else:
            self.rank = int(res.group(2)[0])
            spin_type = self.rank - (ord(res.group(2)[1]) - 65)

        indices = res.group(3)

        lines = []
        for i, indx in enumerate(indices):
            if (i % self.rank) - spin_type < 0:
                lines.append(Line(indx, 'a'))
            else:
                lines.append(Line(indx, 'b'))

        assert len(lines) / 2 == self.rank, "lines don't match rank"
        self.lines = lines

    def _decode_hbar(self, res):

        self.name = res.group(2)
        alpha = re.compile(r'([a-z]+)')
        beta = re.compile(r'\\tilde{([a-z])}')

        self.bra = res.group(1)
        self.ket = res.group(3)

        lines = []
        res = beta.findall(self.bra + self.ket)
        for indx in res:
            lines.append(Line(indx, 'b'))

        rem_beta = beta.sub("", self.bra + self.ket)
        res = alpha.search(rem_beta)
        if res:
            for indx in res.group(1):
                lines.append(Line(indx, spin='a'))


        self.rank = int(len(lines) / 2)
        self.lines = lines

    def has_line(self, indx):
        for line in self.lines:
            if indx == line.indx:
                return True
        return False

    def swap_line(self, source_line, target_line):
        for i, line in enumerate(self.lines):
            if source_line.indx == line.indx:
                spin = line.spin
                self.lines[i] = target_line
                self.lines[i].spin = spin

    def del_line(self, indx):
        for line in self.lines:
            if line.indx == indx:
                self.lines.remove(line)

    def get_line(self, occ_type, internal):
        for line in self.lines:
            if line.occ_type == occ_type and line.internal == internal:
                return line

    def get_spin(self):
        cnt_spin = 0
        for line in self.lines:
            if line.spin == 'b':
                cnt_spin += 1

        cnt_spin = int(cnt_spin / 2)
        return CONFIG['spins'][cnt_spin]

    def __bool__(self):
        if len(self.lines) == 0:
            return False
        else:
            return True


    def __repr__(self):
        indices = []
        for line in self.lines:
            indices.append(line.indx)

        if 'F' in self.name or 'V' in self.name:
            return str("{}_{{{}}}({})".format(self.name, self.get_spin(), ''.join(indices)))
        else:
            return str("{}_{{{}{}}}({})".format(self.name, self.rank,
                self.get_spin(), ''.join(indices)))

class Equation:

    def __init__(self, string):
        self.operator = Operator(string)
        if not self._check_operator():
            raise RuntimeError(self.operator.string + " cannot have repeated indices.")
        self.terms = []


    def append(self, term):
        assert isinstance(term, Term)
        self.terms.append(term)

    def _check_operator(self):
        lines = [x.indx for x in self.operator.lines]
        uniq = set(lines)

        res = len(lines) == len(uniq)

        return res

    def contract(self):
        for term in self.terms:
            term.contract_r()
        self.factor()

    def factor(self):
        test_factor = [term.contraction.frac < 1 for term in self.terms]
        if all(test_factor):
            for term in self.terms:
                term.contraction.frac /= Fraction(1,2)
                self.factor()
        #all(for term in self.

    def _get_id(self):
        right_cnt = 0
        right_cnt_spin = 0
        left_cnt = 0
        left_cnt_spin = 0

        for line in self.operator.lines:
            if not line.internal:

                if line.occ_type == 'o':
                    right_cnt += 1
                    if line.spin == 'b':
                        right_cnt_spin += 1

                elif line.occ_type == 'u':
                    left_cnt += 1
                    if line.spin == 'b':
                        left_cnt_spin += 1

        return "R{}{}{}{}".format(left_cnt,
                CONFIG['spins'][left_cnt_spin],
                right_cnt,
                CONFIG['spins'][right_cnt_spin])

    def __str__(self):
        return str(self._get_id())

    def __repr__(self):
        return str(self._get_id())


class Term:

    def __init__(self, contraction, hamiltonian, right):
        self.hamiltonian = Operator(hamiltonian)
        self.clusters = []

        operators = re.findall(r't_{[\dA-Z]+}\([a-z]+\)', right)
        for operator in operators:
            self.clusters.append(Operator(operator))

        operator = re.search(r'r_{[\dA-Z]+}\([a-z]+\)', right)
        self.right = Operator(operator.group(0))

        self.contraction = Contraction(contraction, self.hamiltonian,
                self.clusters)

        self.contracted = False


    def term_lines(self):
        contraction = re.search(r'{([a-z]+)}', self.contraction)
        string = self.hamiltonian + \
                self.right.split('r_')[0]
        res = re.findall(r'\(([a-z]+)\)', string)
        if contraction:
            res.append(contraction.group(1))

        return res

    def contract_r(self):
        if self.contracted:
            raise Exception("Term cannot be contracted twice.")
        r = self.right
        con = self.contraction
        lines = con.lines.copy()
        for con_line in lines:
            if r.has_line(con_line.indx):
                proj_line = con.get_avail_ext_line(con_line.occ_type)
                self.contraction.lines.remove(con_line)
                self.hamiltonian.swap_line(con_line, proj_line)

        for con_line in con.lines:
            con_line_new = con.get_avail_int_line(con_line.occ_type)
            self.contraction.swap_line(con_line, con_line_new)
            self.hamiltonian.swap_line(con_line, con_line_new)
            for t in self.clusters:
                t.swap_line(con_line, con_line_new)

        self.contracted = True


    def __str__(self):
        if self.contraction == "":
            return self.hamiltonian + self.right
        else:
            return self.contraction + self.hamiltonian + self.right

class Contraction:

    def __init__(self, string, hamiltonian, clusters):
        self.string = string.strip()
        self.sign = string[0]
        res = re.search(r'\\frac{(\d)}{(\d)}', string)
        if res:
            self.frac = Fraction(res.group(1) + "/" + res.group(2))
        else:
            self.frac = Fraction("1")

        res = re.search(r'sum_{([a-z]+)}', string)
        lines = []
        for indx in res.group(1):
            lines.append(Line(indx, 'a'))

        self.lines = lines

        occ_lines = []
        # This stuff is not right
        for indx in Line.PROJ_OCC_ORBS:
            occ_lines.append(Line(indx, 'a'))
            for cluster in clusters:
                if cluster.has_line(indx):
                    occ_lines.pop(-1)
            if hamiltonian.has_line(indx):
                    occ_lines.pop(-1)


        unocc_lines = []
        for indx in Line.PROJ_UNOCC_ORBS:
            unocc_lines.append(Line(indx, 'a'))
            for cluster in clusters:
                if cluster.has_line(indx):
                    unocc_lines.pop(-1)
            if hamiltonian.has_line(indx):
                    unocc_lines.pop(-1)

        self.occ_ext_lines = occ_lines
        self.unocc_ext_lines = unocc_lines

        occ_lines = []
        for indx in Line.CON_OCC_ORBS:
            occ_lines.append(Line(indx, 'a'))

        unocc_lines = []
        for indx in Line.CON_UNOCC_ORBS:
            unocc_lines.append(Line(indx, 'a'))

        self.occ_int_lines = occ_lines
        self.unocc_int_lines = unocc_lines


    def get_avail_ext_line(self, occ_type):
        if occ_type == 'o':
            return self.occ_ext_lines.pop(0)
        elif occ_type == 'u':
            return self.unocc_ext_lines.pop(0)

    def get_avail_int_line(self, occ_type):
        if occ_type == 'o':
            return self.occ_int_lines.pop(0)
        elif occ_type == 'u':
            return self.unocc_int_lines.pop(0)


    def swap_line(self, source_line, target_line):
        for i, line in enumerate(self.lines):
            if source_line.indx == line.indx:
                spin = line.spin
                self.lines[i] = target_line
                self.lines[i].spin = spin

    def del_line(self, indx):
        for line in self.lines:
            if line.indx == indx:
                self.lines.remove(line)

    def __bool__(self):
        if len(self.lines) == 0:
            return False
        else:
            return True

    def __repr__(self):
        indices = []
        for line in self.lines:
            indices += line.indx

        if self.frac == 1:
            pre = self.sign
        else:
            pre = self.sign + \
            "\\frac{{{}}}{{{}}}".format(self.frac.numerator,
                    self.frac.denominator)

        if len(indices) == 0:
            return ""
        else:
            return str(pre + "\\sum_{{{}}}".format(''.join(indices)))



def parse_eqn(lines):
    #hbar_element = re.compile(r'<(.+)\|\\bar{([A-z])}\|(.+)>')
    #spin = re.compile(r'tilde{([a-z]})}|([a-z]+)')

    eqns = []
    eqn = None
    contraction = None
    hamiltonian = None
    right = None
    for line in lines:
        if r"\bar{H}" in line:
            eqn = Equation(line)

        elif eqn:
            if r"\sum" in line and not contraction:
                contraction = line
            elif r"\bar{" in line and not hamiltonian:
                hamiltonian = line
            elif r"r_{" in line and not right:
                right = line
            elif r"nonumber" in line:
                if all([contraction, hamiltonian, right]):
                    eqn.append(Term(contraction, hamiltonian, right))
                contraction = None
                hamiltonian = None
                right = None

        if r"\end{eqnarray}" in line:
            eqns.append(eqn)
            eqn = None
            contraction = None
            hamiltonian = None
            right = None

    return eqns

def create_folder(dir_path):
    try:
        os.mkdir(dir_path)
        print(dir_path + " created")
    except FileExistsError:
        pass

def write_files(out_files):

    create_folder('new_test')
    create_folder('new_test/0')
    create_folder('new_test/1')

    output_files = []
    for out_file, eqns in out_files.items():
        if len(eqns) == 2:
            for i, eqn in enumerate(eqns):
                new_file = "new_test/{}/".format(i) + out_file + ".tex"
                write_file(new_file, eqn)
        elif len(eqns) == 1:
            new_file = "new_test/" + out_file + ".tex"
            write_file(new_file, eqns[0])
        output_files.append(new_file)

    print('\nOutput files:')
    print(output_files)



def write_file(new_file, eqn):

    preamble = r"""\documentclass[a4paper,fleqn,1pt,ccstart]{article}
    \usepackage{txfonts}
    \title{Matrix Element $<?|\bar{H}|?>$}
    \usepackage{fullpage}
    \begin{document}
    \maketitle
     """

    with open(new_file, 'w') as f_out:
        f_out.write(preamble + "\n")
        f_out.write(r"\begin{eqnarray}" + "\n")
        f_out.write(eqn.operator.string + "\n")
        for term in eqn.terms:
            if term.contraction:
                f_out.write(str(term.contraction) + "\n")
            f_out.write(str(term.hamiltonian) + "\n")
            if len(term.clusters) != 0:
                for t in term.clusters:
                    f_out.write(str(t))
                f_out.write("\n")
            f_out.write(r"\nonumber\\&&" + "\n")

        f_out.write(r"\end{eqnarray}" + "\n \n")
        f_out.write(r"\end{document}" + "\n")

def test_print(eqns):
    for eqn in eqns:
        print(eqn.operator.string)
        for term in eqn.terms:
            if term.contraction:
                print(str(term.contraction))
            print(str(term.hamiltonian))
            if len(term.clusters) != 0:
                for t in term.clusters:
                    print(str(t))

            print(r"\nonumber\\&&")
#
        print("\n\n")


def main():
    eqn_files = sys.argv[1:]
    print('Input files:')
    print(eqn_files)
    eqns = []
    for eqn_file in eqn_files:
        with open(eqn_file) as f_eqn:
            f_tmp = f_eqn.readlines()
            eqns += parse_eqn(f_tmp)

    out_files = dict()
    for eqn in eqns:
        eqn.contract()
        try:
            out_files[str(eqn)].append(eqn)
        except KeyError:
            out_files[str(eqn)] = []
            out_files[str(eqn)].append(eqn)

    write_files(out_files)
    #for out_file, eqns in out_files.items():
    #    print(out_file, len(eqns))

if __name__ == "__main__":
    main()


