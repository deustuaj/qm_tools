import sys

def write_full_trips(norb, alpha, beta, confs, ferms, spins):
    with open("t3_confs.dat", "w") as f_out:
        f_out.write("{} {} {}\n".format(len(confs), int(norb) * 2, int(alpha) + int(beta)))
        for line in confs:
            f_out.write(line)

    form ="{:>4}" * 6 + "\n"

    with open("t3_ferms.dat", "w") as f_out:
        for ex in ferms:
            f_out.write(form.format(*ex))

    with open("t3_spins.dat", "w") as f_out:
        for spin in spins:
            f_out.write("{}\n".format(spin))

def write_trips(norb, alpha, beta, confs, ferms, pos, typ):
    with open("t3{}_confs.dat".format(typ), "w") as f_out:
        f_out.write("{} {} {}\n".format(len(confs), int(norb) * 2, int(alpha) + int(beta)))
        for line in confs:
            f_out.write(line)

    form ="{:>4}" * 6 + "\n"

    with open("t3{}_ferms.dat".format(typ), "w") as f_out:
        for ex in ferms:
            f_out.write(form.format(*ex))

    with open("t3{}_pos.dat".format(typ), "w") as f_out:
        f_out.write("{}\n".format(len(pos)))
        for i in pos:
            f_out.write("{}\n".format(i))

with open(sys.argv[1]) as f:
    ref = set(range(1,int(sys.argv[2]) + 1))
    t3_confs = []
    t3_ferm = []
    t3_spins = []
    t3_a = []
    t3_b = []
    t3_c = []
    t3_d = []
    t3_a_ferm = []
    t3_b_ferm = []
    t3_c_ferm = []
    t3_d_ferm = []
    t3_a_pos = []
    t3_b_pos = []
    t3_c_pos = []
    t3_d_pos = []

    cnt_t3 = 1

    for n, i in enumerate(f):
        ex = [int(x) for x in i.split()]
        ex = set(ex)

        ex_to = ex - ref
        ex_from = ref - ex

        if len(ex_to) == 3:

            cnt_a = 0
            alpha_from = []
            beta_from = []
            alpha_to = []
            beta_to = []
            for orb in ex_to:
                if orb % 2 == 1:
                    cnt_a += 1
                    alpha_to.append(int(orb/2) + 1)
                else:
                    beta_to.append(int(orb/2))

            for orb in ex_from:
                if orb % 2 == 1:
                    alpha_from.append(int(orb/2) + 1)
                else:
                    beta_from.append(int(orb/2))

            alpha_to.sort(reverse=True)
            alpha_from.sort(reverse=True)
            beta_to.sort(reverse=True)
            beta_from.sort(reverse=True)

            t3_confs.append(i)

            if cnt_a == 3:
                t3_a.append(i)
                t3_a_ferm.append(alpha_to + alpha_from)
                t3_ferm.append(alpha_to + alpha_from)
                t3_a_pos.append(cnt_t3)
                t3_spins.append(1)
            elif cnt_a == 2:
                t3_c.append(i)
                t3_c_ferm.append([alpha_to[0]] + beta_to + \
                        [alpha_to[1]] + [alpha_from[0]] + beta_from + \
                        [alpha_from[1]])
                t3_ferm.append([alpha_to[0]] + beta_to + \
                        [alpha_to[1]] + [alpha_from[0]] + beta_from + \
                        [alpha_from[1]])
                t3_c_pos.append(cnt_t3)
                t3_spins.append(3)
            elif cnt_a == 1:
                #print(beta_to)
                t3_d.append(i)
                t3_d_ferm.append(beta_to + alpha_to + \
                        beta_from + alpha_from )
                #print(beta_to + alpha_to + \
                #        beta_from + alpha_from )
                t3_ferm.append(beta_to + alpha_to + \
                        beta_from + alpha_from)
                t3_d_pos.append(cnt_t3)
                t3_spins.append(4)
            elif cnt_a == 0:
                t3_b.append(i)
                t3_b_ferm.append(beta_to + beta_from)
                t3_ferm.append(beta_to + beta_from)
                t3_b_pos.append(cnt_t3)
                t3_spins.append(2)

            cnt_t3 += 1


print('t3a', len(t3_a_ferm), len(t3_a))
print('t3b', len(t3_b_ferm), len(t3_b))
print('t3c', len(t3_c_ferm), len(t3_c))
print('t3d', len(t3_d_ferm), len(t3_d))


with open("start.inp") as f:
    dat_line = f.readline()
    alpha, beta, norb = dat_line.split()

    print(alpha, beta, norb)

write_full_trips(norb, alpha, beta, t3_confs, t3_ferm, t3_spins)

#write_trips(norb, alpha, beta, t3_a, t3_a_ferm, t3_a_pos, 'a')
#write_trips(norb, alpha, beta, t3_b, t3_b_ferm, t3_b_pos, 'b')
#write_trips(norb, alpha, beta, t3_c, t3_c_ferm, t3_c_pos, 'c')
#write_trips(norb, alpha, beta, t3_d, t3_d_ferm, t3_d_pos, 'd')

#f_out = open("trip_confs.dat", "w")
#f_out.write("{} {} {}\n".format(len(trips), int(norb) * 2, int(alpha) + int(beta)))
#
#f_out.close()

