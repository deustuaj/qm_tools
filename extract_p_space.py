#!/usr/bin/env python3

import argparse
import math
import sys


def normal_extraction(args):
    """
    Extract determinants from reports from walkers a list file.
    The walker list file is written by the modified Hande program and contains
    lists of determinants organized in reports.
    """

    # Loop over walker files
    for current_file in args.file:
        f_walkers = open(current_file,'r')
        for target_report in args.reports:
            start_flag = False
            f_target = open('ewalkers-{:04d}'.format(target_report), 'a')
            for line in f_walkers:
                if 'ireport' in line:
                    test = line.split()
                    if int(test[2]) ==  target_report:
                        start_flag = True
                        continue

                # Skip idet line
                if 'idet' in line:
                    continue

                # Skip iattempt line
                if 'iattempt' in line:
                    continue

                # Skip i_excitor line
                if 'i_excitor' in line:
                    continue

                # Skip icycle line
                if 'icycle' in line:
                    continue

                # Skip number of states
                if 'nstates' in line:
                    continue

                # Skip thread id of
                if 'thread_id' in line:
                    continue

                # Stop recording determinants
                if start_flag == True and ('End report dump' in line):
                    start_flag = False
                    break

                # Write determinants to output file
                if start_flag == True:
                    f_target.write(line)
            f_target.close()
        f_walkers.close()


def mpi_extraction(args):
    """
    Extract determinants from reports from walkers list files from each MPI process.
    The walker list file is written by the modified Hande program and contains
    lists of determinants organized in reports.
    """

    # Loop over walker files
    for current_file in args.file:
        print("Processing file: {}.".format(current_file))
        f_walkers = open(current_file,'r')
        for target_report in args.reports:
            print("  Extracting report: {}.".format(target_report))
            with open('ewalkers-{:04d}'.format(target_report), 'a') as f_target:
                start_flag = False
                # Find starting line
                for line in f_walkers:
                    if 'ireport' in line:
                        test = line.split()
                        if int(test[2]) ==  target_report:
                            start_flag = True
                            continue

                    # Skip idet line
                    if 'idet' in line:
                            continue

                    # Skip iattempt line
                    if 'iattempt' in line:
                        continue

                    # Skip i_excitor line
                    if 'i_excitor' in line:
                        continue

                    # Skip icycle line
                    if 'icycle' in line:
                        continue

                    # Skip number of states
                    if 'nstates' in line:
                        continue

                    # Skip thread id of
                    if 'thread_id' in line:
                        continue

                    # Stop recording determinants
                    if (start_flag == True) and ('End report dump' in line):
                        start_flag = False
                        break

                    # Write determinants to output file
                    if start_flag == True:
                        f_target.write(line)
            f_target.close()
        f_walkers.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Hande Determinant extractor.')
    parser.add_argument("--mpi", help='MPI walker list.', action='store_true')
    parser.add_argument("-r", "--reports", nargs='+', type=int, required=True,
                        help="Target walker ireports written by Hande.")
    parser.add_argument("file", nargs='+', type=str, help='Input file(s) containing the walkers lists.')

    args = parser.parse_args()
    if args.mpi:
        mpi_extraction(args)
    else:
        normal_extraction(args)
