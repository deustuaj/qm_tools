import sys

files = []
for filename in sys.argv[1:]:
    with open(filename) as f_in:
        files.append((filename, f_in.readlines()))

new_files = []
for filename, lines in files:
    new_lines = []
    changed = False
    for line in lines:
        new_line = line
        if line[0] == " ":
            com = line.find("!")
            unt_com = line[0:com]
            if len(unt_com) > 72:
                b_point = unt_com.rfind(",",0,72) + 1
                first = unt_com[0:b_point] + "\n"
                second = " "*5 + "& " + unt_com[b_point:] + line[com:]
                new_line = first+second
                changed = True
        new_lines.append(new_line)

    if changed:
        new_files.append((filename, new_lines))

for filename, lines in new_files:
    with open(filename, "w") as f_out:
        for line in lines:
            if not "\n" in line:
                f_out.write(line + "\n")
            else:
                f_out.write(line)



