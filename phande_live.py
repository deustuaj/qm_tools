#!/usr/bin/env python3

import time, random
import math
from collections import deque
import matplotlib.pyplot as plt
import os
import sys

start = time.time()

class RealtimePlot:
    def __init__(self, axes, max_entries = 100):
        self.axis_x = deque(maxlen=max_entries)
        self.axis_y = deque(maxlen=max_entries)
        self.axes = axes
        self.max_entries = max_entries

        self.lineplot, = axes.plot([], [], "ro-")
        self.axes.set_autoscaley_on(True)

    def add(self, x, y):
        self.axis_x.append(x)
        self.axis_y.append(y)
        self.lineplot.set_data(self.axis_x, self.axis_y)
        self.axes.set_xlim(self.axis_x[0], self.axis_x[-1] + 1e-15)
        self.axes.relim()
        self.axes.autoscale_view() # rescale the y-axis

    def animate(self, figure, callback, interval = 50):
        import matplotlib.animation as animation
        def wrapper(frame_index):
            self.add(*callback(frame_index))
            self.axes.relim()
            self.axes.autoscale_view() # rescale the y-axis
            return self.lineplot
        animation.FuncAnimation(figure, wrapper, interval=interval)

def main():

    #fig, axes = plt.subplots()
    #display = RealtimePlot(axes)
    #display.animate(fig, lambda frame_index: (0, 0))
    #plt.show()

    fig, axes = plt.subplots()
    display = RealtimePlot(axes)

    f = open(sys.argv[1],"r")
    stat = os.stat(sys.argv[1])
    f.seek(stat.st_size)
    while True:
        where = f.tell()
        line = f.readline()
        if not line:
            time.sleep(1)
            f.seek(where)
        else:
            try:
                fields = line.split()
                corr_e = float(fields[2])/float(fields[3])
                display.add(int(fields[0]), corr_e )
                plt.pause(0.001)
            except:
                pass

if __name__ == "__main__": main()
