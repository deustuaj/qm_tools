import sys

with open(sys.argv[1], 'r') as f_in:
    lines = f_in.readlines()
f_in.close()

dup_lines = []
counter = 0
for i, line_up in enumerate(lines):
    if i in dup_lines:
        continue
    print(line_up,end="")
    for ii, line_down in enumerate(lines[i+1:]):
        if line_up == line_down:
            dup_lines.append(i+ii+1)
    counter += 1

#print(dup_lines)
print(counter)

