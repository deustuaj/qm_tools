#!/bin/bash

echo 'Wall times'
for i in $*;
do
    grep -e 'Wall time' $i
done
