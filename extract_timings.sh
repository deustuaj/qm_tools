#!/bin/bash

echo 'CI spaces'
echo '---------'
#for report in "${reports[@]}"; do
for report in $@; do
    rep=$(echo $report | grep -o "[0-9]\+")
    iter=$(echo "$rep*20" | bc)
    grep -e "^\s\+$iter " $report | sed 's/\s\+/ /g' | cut -d ' ' -f 7
done

echo ''
echo 'Timings'
echo '-------'
for report in $@; do
    grep -e 'Wall time' $report | cut -d ':' -f 2 | sed 's/^\s\+//g'
done


