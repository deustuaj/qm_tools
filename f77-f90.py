import sys
import re
#from fprettify import reformat_inplace

newlines = []
sub_res = re.compile("(subroutine|function|program) ([\d\w]+)\(?")
sub_real = re.compile("real\*8")
sub_integer = re.compile("integer")
with open(sys.argv[1]) as f:
    lines = f.readlines()

    for indx, line in enumerate(lines):
        try:
            next_line = lines[indx+1]
        except IndexError:
            pass

        res = sub_res.search(line)
        if res:
            endsub = res.group(1)
            sub = res.group(2)
            print(endsub, sub)

        if not "allocatable" in line:
            line = sub_real.sub("real(kind=8) ::", line)
        else:
            line = sub_real.sub("real(kind=8)", line)
        line = sub_integer.sub("integer ::", line)

        if '&' in next_line:
            newlines.append((indx, line.replace('&', '').replace('\n', ' &\n')))
        elif '&' in line:
            newlines.append((indx, line.replace('&', '')))
        else:
            if line[0:2] == 'C\n':
                newlines.append((indx, line.replace('C', '')))
            elif line[0] == 'C':
                newlines.append((indx, line.replace('C', '!')))
            elif " end\n" in line:
                newlines.append((indx, "end {} {}\n".format(endsub, sub)))
            else:
                newlines.append((indx, line))


with open(sys.argv[2], 'w') as f_out:
    for indx, line in enumerate(newlines):
        assert indx == line[0], 'Index mismtach'

        f_out.write(line[1])
        #print(line[1],end="")

#reformat_inplace(sys.argv[2])




