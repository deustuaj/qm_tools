#!/bin/bash

if [[ $1 == '-f' ]]; then
    A="$@"
    files="${A[@]:3}"
    echo 'Total Energy'
    echo '------------'

    echo 'CC(P)'
    for i in $files
    do
            grep -e 'EOM-CCSDt=' ${i} | sed 's/\s\+/ /g' | cut -d ' ' -f 3 | tail -1
    done

    echo ''
    echo 'Q,A'
    for i in $files
    do
            grep -e 'CC(t;3)A' ${i} | sed 's/\s\+/ /g' | cut -d ' ' -f 3 | tail -1
    done

    echo ''
    echo 'Q,D'
    for i in $files
    do
    if [[ $i == '-f' ]]; then
        continue
    fi
            grep -e 'CC(t;3)D' ${i} | sed 's/\s\+/ /g' | cut -d ' ' -f 3 | tail -1
    done
    echo ''

    echo '% of Triples'
    echo '------------'
    for i in $files
    do
    if [[ $i == '-f' ]]; then
        continue
    fi
            grep -e 'nonzero t3 ' ${i} | sed 's/\s\+/ /g' | cut -d ' ' -f 5 | tail -1
    done
else
    echo 'Total Energy'
    echo '------------'

    echo 'CC(P)'
    for i in $@
    do
            grep -e 'EOM-CCSDt=' ${i}/*.out | sed 's/\s\+/ /g' | cut -d ' ' -f 3 | tail -1
    done

    echo ''
    echo 'Q,A'
    for i in $@
    do
            grep -e 'CC(t;3)A' ${i}/*.out | sed 's/\s\+/ /g' | cut -d ' ' -f 3 | tail -1
    done

    echo ''
    echo 'Q,D'
    for i in $@
    do
            grep -e 'CC(t;3)D' ${i}/*.out | sed 's/\s\+/ /g' | cut -d ' ' -f 3 | tail -1
    done
    echo ''

    echo '% of Triples'
    echo '------------'
    for i in $@
    do
            grep -e 'nonzero t3 ' ${i}/*.out | sed 's/\s\+/ /g' | cut -d ' ' -f 5 | tail -1
    done
fi
