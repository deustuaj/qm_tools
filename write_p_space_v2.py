#!/usr/bin/env python3

import argparse
import math
import sys
import itertools as it

def write_excitation(unocc, occ, f_handle):
    outstring = '{:>3}'*(len(unocc)+len(occ))
    for p_unocc in it.permutations(unocc):
        for p_occ in it.permutations(occ):
            f_handle.write(outstring.
                       format(*list(p_unocc),*list(p_occ)))
            f_handle.write("\n")

def spin_writer(unocc_spin, occ_spin, outstring_spin, f_handle):
    occ_spin = [i - 4 for i in occ_spin]
    unocc_spin = set(unocc_spin)
    occ_spin = set(occ_spin)
    if len(occ_spin) == 4 and len(unocc_spin) == 4:
        f_handle.write(outstring_spin.
               format(*unocc_spin,*occ_spin))
        f_handle.write("\n")


def write_spin(unocc, occ, nel, f_handle):
    outstring_spin = '{:>4}'*(len(occ)+len(unocc))
    # bbbb
    unocc_spin = [2*i - nel for i in unocc]
    occ_spin = [2*i for i in occ]
    spin_writer(unocc_spin, occ_spin, outstring_spin, f_handle)

    # aaaa
    unocc_spin = [(2*i-1) - nel for i in unocc]
    occ_spin = [(2*i-1) for i in occ]
    spin_writer(unocc_spin, occ_spin, outstring_spin, f_handle)

    # baaa
    unocc_spin = [2*unocc[0] - nel] + \
        [(2*i-1) - nel for i in unocc[1:]]
    occ_spin = [2*occ[0]] + \
            [(2*i-1) for i in occ[1:]]
    spin_writer(unocc_spin, occ_spin, outstring_spin, f_handle)

    # bbaa
    unocc_spin = [(2*j) - nel for j in \
            unocc[0:2]] + \
        [(2*i-1) - nel for i in unocc[2:]]
    occ_spin = [(2*j) for j in occ[0:2]] + \
            [(2*i-1) for i in occ[2:]]
    spin_writer(unocc_spin, occ_spin, outstring_spin, f_handle)

    # bbba
    unocc_spin = [(3*j) - nel for j in \
            unocc[0:3]] + \
        [(2*i-1) - nel for i in unocc[3:]]
    occ_spin = [(2*j) for j in occ[0:3]] + \
            [(2*i-1) for i in occ[3:]]
    spin_writer(unocc_spin, occ_spin, outstring_spin, f_handle)


def main(args):
    for in_file in args.files:
        print("Processing file: {}.".format(in_file))
        excitation = args.excitations # p-space rank
        frozen = args.frozen

        try:
            f_walkers = open(in_file,'r') # Open walkers file
        except FileNotFoundError:
            print("{} not found!".format(in_file))
            sys.exit(1)

        nel = args.electrons
        f_target = open("{}-p".format(in_file),'w')
        if excitation == 4:
            #print("  Creating spin-version for CC PACKAGE")
            f_target_spin = open("{}-p-spin".format(in_file),'w')
        # Reference occupation numbers not considering frozen electron
        ref = set(range(1,nel + 1))

        # Spatial excitation holder for UCCSDT-stoch
        aaa = []
        aab = []
        abb = []
        bbb = []

        # Spin excitation holder for Q-space correction
        spin_out = []

        for line in f_walkers:
            fields = line.split()
            occupation = set(map(int,fields[2:])) # Determinant occupation
            matches = (set(ref) & set(occupation)) # Insersection

            # Match triple excitation
            if len(matches) == len(ref) - excitation:
                # Unoccupied orbitals (excitations to)
                unocc_spin = occupation - matches

                # Occupied orbitals (excitations from)
                occ_spin = ref - matches

                # Count alpha electrons in a FCIQMC excited determinant
                alpha = 0
                for i in occ_spin:
                    if i % 2 == 1:
                        alpha += 1

                # Format string
                outstring = '{:>3}'*(len(occ_spin)+len(unocc_spin))
                outstring_spin = '{:>4}'*(len(occ_spin)+len(unocc_spin))

                # Convert to spatial
                occ = [math.ceil(x/2 + frozen) for x in occ_spin]
                unocc = [math.ceil(x/2 + frozen) for x in unocc_spin]

                # Spin case selection and permutation
                # Create t3A (format aaa aaa)
                #if excitation == 3:
                    # Create t3B (format bbb bbb)
                    # Create t3A (format aaa aaa)
                    # Create t3C (format aba aba)
                    # Create t3D (format bba bba)
                #write_excitation(unocc, occ, f_target)
                for p_unocc in it.permutations(unocc):
                    for p_occ in it.permutations(occ):
                        f_target.write(outstring.
                                   format(*list(p_unocc),*list(p_occ)))
                        f_target.write("\n")
                        #if excitation == 4:
                        #    write_spin(p_unocc, p_occ, nel + 2*frozen,
                        #        f_target_spin)


        f_walkers.close()
        f_target.close()
        if excitation == 4:
            f_target_spin.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Excitation extractor')
    parser.add_argument("-o", "--orbital-order", action='store_true',
                        help='Read and use orbital order from sp_eigv_order \
                        produced by HANDE.')
    parser.add_argument("-e", "--excitations", type=int, default=3,
                        help='Excitation level.')
    parser.add_argument("-f", "--frozen", type=int, default=0,
                        help='Number of frozen spatial orbitals.')
    parser.add_argument("-n", "--electrons", type=int,
                        help='Number of active electrons \
                        (number of electrons - 2*(frozen spatial).',
                        required=True)
    parser.add_argument("-s", "--spin", action='store_true',
                        help='Separate p space in spin cases.')
    parser.add_argument("files", nargs='+', help='Input files.')
    args = parser.parse_args()
    main(args)
